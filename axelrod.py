import matplotlib.pyplot as plt
import itertools
import random
import copy
import numpy as np

#Needed for display animation
from IPython.display import display, clear_output
import matplotlib.pylab as plt
import time  

class Axelrod:
    '''The Axelrod model of cultural dissemination of culture (1997) uses the concept of 'culture' to denote "the set of individual attributes that are subject to social influence". Axelrod argues that culture "is something people learn from each other", and hence something that evolves through social influence. To study the process of cultural diffusion Axelrod builds a model based on two simple assumptions:

1. Agents are more likely to interact with others who share many of their cultural attributes, 
2. Agent interactions tend to increase the number of cultural attributes they share (thus making them more likely to interact again).'''
    num_attributes = 3
    attribute_values = 10
    
    def __init__(self, width=10, height=10):
        '''Inicialize the Axelrod world. Default values 
           World size = width=10, height=10'''
        self.width = width 
        self.height = height 
        self.agents = {}
        self.populate()

        
    def populate(self):
        '''Inicialize the world with random agents which consist of a random vector of cultural attributes'''
        self.all_houses = list(itertools.product(range(self.width),range(self.height)))
        for i in self.all_houses:
            features = []
            for j in range(0,self.num_attributes):
                features.append(random.randint(0,self.attribute_values-1))
            self.agents[i]=features

    def update(self, n_iterations=10, fig=""):
        ''' Run n_iterations of the simulation by picking random agents and random neighbors. Passing in the "fig" attribute will show the animation.'''
        for i in range(n_iterations):
            n_changes = 0
            n_tries = 100
            old_pos = ''
            new_pos = ''
            color = 'green'
            while(n_changes == 0 and n_tries > 0):
                n_tries -= 1
                #Pick a random agent
                x = random.randint(0,self.width-1)
                y = random.randint(0,self.height-1)
                agent = (x,y)
                if agent in self.agents:
                    #pick a random neighbor
                    change = random.choice([(-1,0), (1,0), (0,-1), (0,1)])
                    n = (agent[0]-change[0], agent[1]-change[1])
                    if n in self.agents:
                        t1 = np.array(self.agents[agent])
                        t2 = np.array(self.agents[n])
                        prob = (t1==t2).sum()/self.attribute_values
                        change_made = 0
                        for f in range(0,self.num_attributes):
                            if t1[f] != t2[f] and random.random() < prob:
                                t1[f] = t2[f]
                                #print("changes made")
                                change_made += 1
                        if(change_made > 0):
                            old_pos = agent
                            new_pos = n
                            n_changes += 1
                            self.agents[agent] = t1
                        else:
                            color = 'red'

            if(fig != ""):
                self.plot('Axelrod model', fig=fig)
                
                if(new_pos != '' and old_pos != ''):
                    plt.plot([old_pos[0]+0.5,new_pos[0]+0.5], [old_pos[1]+0.5,new_pos[1]+0.5],linewidth=5.0, c=color)
                time.sleep(0.0001)         # Sleep for half a second to slow down the animation
                
                # Animaiton part (dosn't change)
                clear_output(wait=True) # Clear output for dynamic display
                display(fig)            # Reset display
                fig.clear()             # Prevent overlapping and layered plots

        
    def plot(self, title = 'Axelrod Simulation', fig=""):
        '''Plot the current state of the axelrod world.  This function allows the user to pass in a title.  The plot function returns a fig object that can be used to pass to the update function and animate the simulation.'''
        if (fig == ""):
            fig, ax = plt.subplots(figsize=(20,10))

        #If you want to run the simulation with more than 7 colors, you should set agent_colors accordingly
        for agent in self.agents:
            x = self.agents[agent]
            
            rgb = [ x[0]/self.attribute_values, x[1]/self.attribute_values, x[2]/self.attribute_values]
            plt.scatter(agent[0]+0.5, agent[1]+0.5, facecolors=rgb, s=1000)

        plt.title(title, fontsize=10, fontweight='bold')
        plt.xlim([0, self.width])
        plt.ylim([0, self.height])
        plt.xticks([])
        plt.yticks([])
        plt.axis('equal')
        return fig